# Copyright 2012 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gsettings
require vala [ with_opt=true option_name=vapi vala_dep=true  ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

SUMMARY="GTK+ support library for colord"
HOMEPAGE="http://www.freedesktop.org/software/colord"
DOWNLOADS="${HOMEPAGE}/releases/${PNV}.tar.xz"

LICENCES="LGPL-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="doc gobject-introspection vapi [[ requires = [ gobject-introspection ] ]]"

DEPENDENCIES="
    build:
        virtual/pkg-config
        dev-util/intltool[>=0.35.0]
        sys-devel/gettext[>=0.17]
        dev-libs/libxslt
        doc? ( dev-doc/gtk-doc[>=1.9] )
    build+run:
        dev-libs/glib:2[>=2.28.0]
        x11-libs/gtk+:3
        sys-apps/colord[>=1.4.1][vapi?]
        media-libs/lcms2[>=2.2]
        gobject-introspection? ( gnome-desktop/gobject-introspection[>=0.9.8] )
"

RESTRICT="test" # requires X

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-gtk2
    --disable-static
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'doc gtk-doc' 'gobject-introspection introspection' 'vapi vala' )

src_prepare() {
    autotools_src_prepare
    edo intltoolize --force --automake
}

