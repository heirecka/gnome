# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require vala [ vala_dep=true with_opt=true option_name=vapi ]

SUMMARY="HTTP library implemented in C"
HOMEPAGE="http://live.gnome.org/LibSoup"

LICENCES="LGPL-2"
SLOT="2.4"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    vapi [[ requires = [ gobject-introspection ] ]]
    ( linguas: an as be bg bn_IN ca ca@valencia cs da de el en_GB eo es et eu fa fr fur gl gu he hi
               hu id it ja kn ko lt lv ml mr nb nl or pa pl pt pt_BR ro ru sk sl sr sr@latin sv ta
               te tg th tr ug uk uz@cyrillic vi zh_CN zh_HK zh_TW )
"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.35.0]
        sys-devel/gettext
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gtk-doc[>=1.20] )
    build+run:
        dev-db/sqlite:3
        dev-libs/glib:2[>=2.38.0]
        dev-libs/libxml2:2.0
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.5] )
    run:
        dev-libs/glib-networking[ssl(+)]
"

RESTRICT="test" # network violations

DEFAULT_SRC_CONFIGURE_PARAMS=(
    # To make glib-networking a runtime dependency
    # Also ease cross-compiling a bit
    --disable-tls-check

    '--without-gssapi'
    '--without-ntlm-auth'

    # NOTE(compnerd) this *must* be enabled to provide backwards compatibility with existing
    # applications.  It consists entirely of deprecated APIs and should not be used in newly
    # written code.
    '--with-gnome'
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    gtk-doc
    'gobject-introspection introspection'
    'vapi vala'
)

